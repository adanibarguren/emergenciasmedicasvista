import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    familiares:[],
    usuario: null
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
