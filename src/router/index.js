import Vue from 'vue'
import VueRouter from 'vue-router'
import EmergenciasMedicas from '../views/EmergenciasMedicas.vue'
import AdministracionDeCategorias from '../views/AdministracionDeCategorias.vue'
import AdministracionEmergenciasMedicas from '../views/AdministracionEmergenciasMedicas.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: EmergenciasMedicas
  },

  {
    path: '/AdministracionDeCategorias',
    name: 'AdministracionDeCategorias',
    component: AdministracionDeCategorias
  },

  {
    path: '/AdministracionEmergenciasMedicas',
    name: 'AdministracionEmergenciasMedicas',
    component: AdministracionEmergenciasMedicas
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
