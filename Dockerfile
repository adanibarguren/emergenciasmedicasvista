# etapa de compilación
FROM node:10-alpine as build-stage
WORKDIR /
COPY package*.json ./
RUN npm install
RUN npm install cors
COPY . .
RUN npm run build

# etapa de producción
FROM nginx:1.13.12-alpine as production-stage
COPY --from=build-stage /dist /usr/share/nginx/html
COPY /config/default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]